import { Component, OnInit } from '@angular/core';
import { DoctorService } from '../../shared/doctor.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
  // providers:[DoctorService]
})
export class SignUpComponent implements OnInit {

  emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  showSucessMessage : boolean;
  serverErrorMessages : String;

  constructor(public doctorService: DoctorService, public routeur : Router ) { }

  ngOnInit(): void { 
  }

  onSubmit(form : NgForm){
    this.doctorService.postDocter(form.value).subscribe(
      res => {
        this.routeur.navigateByUrl('/dashboard');
        this.showSucessMessage = true;
        setTimeout(() => this.showSucessMessage = false, 4000);
        this.resetForm(form);
        
      },
      err => {
        if (err.status === 422) {
          this.serverErrorMessages = err.error.join('<br/>');
        }
        else
          this.serverErrorMessages = 'Something went wrong.Please contact admin.';
      }
    )
  } 

  resetForm(form: NgForm) {
    this.doctorService.selectedDoctor = {
      fullName: '',
      email: '',
      branch: '',
      statut: '',
      phoneNumber:'',
      password: ''
    };
    form.resetForm();
    this.serverErrorMessages = '';
  }
}
