import { Component, OnInit } from '@angular/core';
// import { PatientService } from '../../shared/patient.service';
import { PatientService } from '../../shared/patient.service';
import { NgForm } from '@angular/forms';
import { Router} from '@angular/router';

@Component({
  selector: 'app-add-patient',
  templateUrl: './add-patient.component.html',
  styleUrls: ['./add-patient.component.css']
})
export class AddPatientComponent implements OnInit {

  constructor(public patientService : PatientService , private router : Router) { }

  emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  showSucessMessage : Boolean;
  serverErrorMessages : String;
  
  ngOnInit(): void {
  }
  onSubmit(form : NgForm){
    // var hospitalName = document.getElementsByName('name')
    // var hospitalLocation = document.getElementsByName('location ')
    // const val = this.fruits.length;
    // const varr = [];
    // for(var i=0; i < val; i++ ){
    //   this.hospitalService.selectedHospital.branch.push(this.fruits[i].name)
    // }
    // console.log(varr);
    //Executing the login function in the service file with the user input values we took from the form
    console.log('this is form value', form.value);
    this.patientService.savePatient(form.value).subscribe(
      res => {
        this.showSucessMessage = true;
        setTimeout(() => this.showSucessMessage = false, 4000);
        // this.resetForm(form); 
      },
      err => {
        if (err.status === 422) {
          this.serverErrorMessages = err.error.join('<br/>');
        }
        else
          this.serverErrorMessages = 'Something went wrong.Please contact admin.';
      }
    )
  } 

}
