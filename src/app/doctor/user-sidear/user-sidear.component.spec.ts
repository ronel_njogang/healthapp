import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserSidearComponent } from './user-sidear.component';

describe('UserSidearComponent', () => {
  let component: UserSidearComponent;
  let fixture: ComponentFixture<UserSidearComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserSidearComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserSidearComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
