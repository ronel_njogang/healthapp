import { Component, OnInit } from '@angular/core';
import { UserSidebarService } from 'src/app/shared/user-sidebar.service';
import { Router } from '@angular/router';
 
@Component({
  selector: 'app-user-sidear',
  templateUrl: './user-sidear.component.html', 
  styleUrls: ['./user-sidear.component.css']
})
export class UserSidearComponent implements OnInit {

  constructor(public router : Router , public userSidebarService : UserSidebarService) { }

  ngOnInit(): void {
  }

  addPatient(){
    this.router.navigateByUrl('/add-patient')
  }
  logOut(){
    this.userSidebarService.deleteToken();
    console.log('remove token ?')
    // localStorage.removeItem('token');
    this.router.navigateByUrl('/login')
  }

}
