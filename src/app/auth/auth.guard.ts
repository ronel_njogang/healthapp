import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { DoctorService } from '../shared/doctor.service';
import { AdminService } from '../shared/admin.service';
import { Router } from '@angular/router'
 


@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private doctorService : DoctorService, private adminService :AdminService, private router : Router ){

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
      if(!this.doctorService.isLoggedIn()){
        this.router.navigateByUrl('/login');
        this.doctorService.deleteToken();
      }
    return true;
  }
  
}
