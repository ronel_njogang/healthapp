import { Routes } from '@angular/router';
// import { DoctorComponent } from './doctor/doctor.component';
import { SignUpComponent } from './doctor/sign-up/sign-up.component';
import { SignInComponent } from './doctor/sign-in/sign-in.component';
import { UserProfileComponent } from './user-profile/user-profile.component';

import { AuthGuard } from './auth/auth.guard'
import { WelcomeComponent } from './welcome/welcome.component';
import { AdminComponent } from './admin/admin.component';
import { AdminSigninComponent } from './admin/signin/signin.component';
import { AdminSignupComponent } from './admin/signup/signup.component';
import { AdminDashboardComponent } from './admin/dashboard/dashboard.component';
import { AddHospitalComponent } from './admin/add-hospital/add-hospital.component';
import { DashboardComponent } from './doctor/dashboard/dashboard.component'
import { AddPatientComponent } from './doctor/add-patient/add-patient.component';



export const appRoutes: Routes = [

    {
        path: '', component: WelcomeComponent,
        // children: [{ path: '', component: SignUpComponent }]
    },

    {
        path: 'admin', component: AdminComponent,
        children: [
            { path: 'signin', component: AdminSigninComponent },
            { path: 'dashboard', component: AdminDashboardComponent},
            { path: '', component: AdminSignupComponent },
            { path: 'add-hospital', component: AddHospitalComponent},
]
    } ,

    {
        path: 'signup', component: SignUpComponent,
        // children: [{ path: '', component: SignUpComponent }]
    },
 
    {
        path: 'login', component: SignInComponent,
        // children: [{ path: '', component: SignInComponent }]
    },

    {
        path: 'userprofile', component: UserProfileComponent,
    },

    {
        path: 'dashboard', component: DashboardComponent,canActivate:[AuthGuard]
    },
    {
        path: 'add-patient', component: AddPatientComponent,
    }

    // {
    //     path: '', redirectTo: '/login', pathMatch: 'full'
    // }
];