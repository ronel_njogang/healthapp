//built in
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormGroup, FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatChipInputEvent} from '@angular/material/chips';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import { NoopAnimationsModule, BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatChipsModule} from '@angular/material/chips';
import {MatNativeDateModule} from '@angular/material/core';


//components
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DoctorComponent } from './doctor/doctor.component';
import { SignUpComponent } from './doctor/sign-up/sign-up.component';
import { UserProfileComponent } from './user-profile/user-profile.component';


//routes
import { appRoutes } from './routes';
import { SignInComponent } from './doctor/sign-in/sign-in.component'

//services
import { DoctorService } from './shared/doctor.service';
import { HospitalService } from './shared/hospital.service';

//models
import { Hospital } from './shared/hospital.model';
import { Patient } from './shared/patient.model';


//others
import { AuthGuard } from './auth/auth.guard';
import { AdminComponent } from './admin/admin.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { AdminSigninComponent } from './admin/signin/signin.component';
import { AdminSignupComponent } from './admin/signup/signup.component';
import { AdminSidebarComponent } from './admin/sidebar/sidebar.component';
import { AdminDashboardComponent } from './admin/dashboard/dashboard.component';
import { AddHospitalComponent } from './admin/add-hospital/add-hospital.component';
import { DashboardComponent } from './doctor/dashboard/dashboard.component';
import { LogOutComponent } from './admin/log-out/log-out.component';
import { UserSidearComponent } from './doctor/user-sidear/user-sidear.component';
import { AddPatientComponent } from './doctor/add-patient/add-patient.component'

@NgModule({
  declarations: [
    AppComponent,
    DoctorComponent,
    SignUpComponent,
    SignInComponent,
    UserProfileComponent,
    AdminComponent,
    WelcomeComponent,
    AdminSigninComponent,
    AdminSignupComponent,
    AdminSidebarComponent,
    AdminDashboardComponent,
    AddHospitalComponent,
    DashboardComponent,
    LogOutComponent,
    UserSidearComponent,
    AddPatientComponent
  ],
  imports: [
    
    MatInputModule,
    MatDatepickerModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    BrowserModule, 
    FormsModule, 
    NoopAnimationsModule, 
    MatChipsModule, 
    BrowserAnimationsModule,
    MatInputModule, ReactiveFormsModule,
    MatNativeDateModule
    
    
    
  ],
  exports: [
    MatDatepickerModule,
    MatInputModule,
    MatFormFieldModule,
    AddHospitalComponent
  ],
  providers: [DoctorService,HospitalService,Hospital,Patient, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
