import { Component, OnInit } from '@angular/core';
import { DoctorService } from '../shared/doctor.service';
import { Router } from '@angular/router'

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

  constructor(private doctorService : DoctorService, private router : Router) { }

  logOut(){
    this.doctorService.deleteToken();
    console.log('remove token ?')
    localStorage.removeItem('token');
    this.router.navigateByUrl('/login')
  }
  ngOnInit(): void {
  }

}
