export class Hospital {
    name: string;
    location: string;
    branch: [string];
    address: string;
}
