import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Patient } from './patient.model';
import { environment } from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class PatientService {

    selectedPatient: Patient = {
      fullName: '',
      address: '',
      phoneNumber: '',
      dateOfBirth: '',
      email: ''

    };
  
    constructor(public http: HttpClient, public patient : Patient) { }
  
    savePatient(patient){
  
      //sending user entries to the register function in the server side by using the api link to server previously defined in environment class + the register path
  
      return this.http.post(environment.apiBaseUrl+'/addPatient', patient)
    };

  
  
} 
