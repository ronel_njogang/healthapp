import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Doctor } from './doctor.model';
import { environment } from '../../environments/environment'


@Injectable({
  providedIn: 'root'
})

export class DoctorService {

  selectedDoctor: Doctor = {
    fullName: '',
    statut: '',
    branch: '',
    phoneNumber: '',
    email: '',
    password: ''
  };
  constructor(public http: HttpClient) { }

    postDocter(doctor : Doctor){

      //sending user entries to the register function in the server side by using the api link to server previously defined in environment class + the register path

      return this.http.post(environment.apiBaseUrl+'/register', doctor)
    }

    login(authCredentials){ 
      //making post request to nodeJs API /authenticate and sending credentials email and password
      //Return type : observable
      return this.http.post(environment.apiBaseUrl+'/authenticate', authCredentials)
    }


    setToken(token: string) {
      localStorage.setItem('token', token);
    }

    deleteToken(){
      localStorage.removeItem('token');
    }

    getDoctorPayload(){
      //Taking the token from localstorage
      var token = localStorage.getItem('token')
      //If the token exist
      if(token){

        //get the token by, encode it to have the tree elements, split them by removing the point and transform the element in string, and get the payload, which is the second 
        var doctorPayload = atob(token.split('.')[1]);
        return JSON.parse(doctorPayload)

      }
      else{
        console.log("An error occurs. We cannot get the token")
        return null
      }
    }

  isLoggedIn() {
    var doctorPayload = this.getDoctorPayload();
    if (doctorPayload)
      return doctorPayload.exp > Date.now() / 1000;
    else
      return false;
  }

  logOut(){
    localStorage.removeItem('token');
  }

  // validateStatut(){
  //   var statut = document.get
  // }
  
} 
