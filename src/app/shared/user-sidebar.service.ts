import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserSidebarService {

  constructor() { } 

  deleteToken(){
    localStorage.removeItem('token');
  };
  logOut(){
    localStorage.removeItem('token');
  }

}
