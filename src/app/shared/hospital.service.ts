import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Hospital } from './hospital.model';
import { environment } from '../../environments/environment'

@Injectable({
  providedIn: 'root' 
})
export class HospitalService {

  selectedHospital: Hospital = {
    name: '',
    location: '',
    branch: [''],
    address: ''

  };

  constructor(public http: HttpClient, public hospital: Hospital) { }

  saveHospital(hospital){

    //sending user entries to the register function in the server side by using the api link to server previously defined in environment class + the register path

    return this.http.post(environment.apiBaseUrl+'/addHospital', hospital)
  };

  logOut(){
    localStorage.removeItem('token');
  }


}
