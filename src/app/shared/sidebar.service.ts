import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SidebarService {

  constructor() { }

    deleteToken(){
      localStorage.removeItem('token');
    };
  logOut(){
    localStorage.removeItem('token');
  }
}
