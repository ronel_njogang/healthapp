import { Component, OnInit } from '@angular/core';
import { DoctorService } from '../../shared/doctor.service';
import { AdminService } from '../../shared/admin.service'
import { NgForm } from '@angular/forms';
import { Router} from '@angular/router';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class AdminSigninComponent implements OnInit {

  constructor(public adminService : AdminService, private routeur : Router) { }

  model = {
    email :'',
    password:''
  };

  emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  // showSucessMessage : boolean;
  serverErrorMessages : String;

  ngOnInit(): void {
  };

  onSubmit(form : NgForm){

    //Executing the login function in the service file with the user input values we took from the form
    this.adminService.login(form.value).subscribe(
      res =>{
        this.adminService.setToken(res['token']);
        this.routeur.navigateByUrl('/admin/dashboard')
      },
      err =>{
        this.serverErrorMessages = err.error.message;
      }

    )
  }

}
