import { Component, OnInit } from '@angular/core';
import { AdminService } from '../../shared/admin.service';
import { NgForm } from '@angular/forms';
import { Router} from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class AdminSignupComponent implements OnInit {

  constructor(public adminService : AdminService, public routeur : Router) { }

  model = {
    email :'',
    password:''
  };

  emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  showSucessMessage : boolean;
  serverErrorMessages : String;

  ngOnInit(): void {
  };

  onSubmit(form : NgForm){
 
    //Executing the login function in the service file with the user input values we took from the form
    this.adminService.postAdmin(form.value).subscribe(
      res => {
        this.showSucessMessage = true;
        setTimeout(() => this.showSucessMessage = false, 4000);
        this.resetForm(form);
        this.routeur.navigateByUrl('/admin/dashboard')
      },
      err => {
        if (err.status === 422) {
          this.serverErrorMessages = err.error.join('<br/>');
        }
        else
          this.serverErrorMessages = 'Something went wrong.Please contact admin.';
      }
    )
  } 

  resetForm(form: NgForm) {
    this.adminService.selectedAdmin = {
      fullName: '',
      email: '',
      password: ''
    };
    form.resetForm();
    this.serverErrorMessages = '';
  }
}
