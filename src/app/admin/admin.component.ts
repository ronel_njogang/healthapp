import { Component, OnInit } from '@angular/core';
import { DoctorService } from '../shared/doctor.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  constructor(public doctorService : DoctorService, private routeur : Router) { }

  model = {
    email :'',
    password:''
  };

  emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  // showSucessMessage : boolean;
  serverErrorMessages : String;

  ngOnInit(): void {
  };

  onSubmit(form : NgForm){

    //Executing the login function in the service file with the user input values we took from the form
    this.doctorService.login(form.value).subscribe(
      res =>{
        this.doctorService.setToken(res['token']);
        this.routeur.navigateByUrl('/userprofile')
      },
      err =>{
        this.serverErrorMessages = err.error.message;
      }

    )
  }

}
