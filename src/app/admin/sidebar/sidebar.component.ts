import { Component, OnInit } from '@angular/core';
// import { AddHospitalComponent } from '../add-hospital/add-hospital.component';
import { SidebarService } from 'src/app/shared/sidebar.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class AdminSidebarComponent implements OnInit {

  constructor( public sidebarService : SidebarService, public router : Router) { }

  ngOnInit(): void {
  }
  addHospital(){
    this.router.navigateByUrl('admin/add-hospital')
  }
  logOut(){
    this.sidebarService.deleteToken();
    console.log('remove token ?')
    // localStorage.removeItem('token');
    this.router.navigateByUrl('admin/signin')
  }

}
