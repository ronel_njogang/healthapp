import { Component, OnInit } from '@angular/core';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {MatChipInputEvent} from '@angular/material/chips';
import { HospitalService } from '../../shared/hospital.service';
import { NgForm } from '@angular/forms';
import { Router} from '@angular/router';

export interface Fruit {
  name: string;
}


@Component({
  selector: 'app-add-hospital',
  templateUrl: './add-hospital.component.html',
  styleUrls: ['./add-hospital.component.css']
})
export class AddHospitalComponent implements OnInit {
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  fruits: Fruit[] = [
    {name: 'General'},
    {name: 'Ophtamologie'},
    {name: 'Pédiatrie'},
  ];
  
  constructor(public hospitalService : HospitalService, private routeur : Router) { }

  showSucessMessage : Boolean;
  serverErrorMessages : String;

  ngOnInit(): void {
  }
  onSubmit(form : NgForm){
    // var hospitalName = document.getElementsByName('name')
    // var hospitalLocation = document.getElementsByName('location ')
    // const val = this.fruits.length;
    // const varr = [];
    // for(var i=0; i < val; i++ ){
    //   this.hospitalService.selectedHospital.branch.push(this.fruits[i].name)
    // }
    // console.log(varr);
    //Executing the login function in the service file with the user input values we took from the form
    console.log('this is form value', form.value);
    this.hospitalService.saveHospital(form.value).subscribe(
      res => {
        this.showSucessMessage = true;
        setTimeout(() => this.showSucessMessage = false, 4000);
        this.resetForm(form); 
      },
      err => {
        if (err.status === 422) {
          this.serverErrorMessages = err.error.join('<br/>');
        }
        else
          this.serverErrorMessages = 'Something went wrong.Please contact admin.';
      }
    )
  } 

  resetForm(form: NgForm) {
    this.hospitalService.selectedHospital = {
      name: '',
      location: '',
      branch: [''],
      address: ''
    };
    form.resetForm();
    this.serverErrorMessages = '';
  }  

  //input tag for hospital department
  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our fruit
    if ((value || '').trim()) {
      this.fruits.push({name: value.trim()});
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  remove(fruit: Fruit): void {
    const index = this.fruits.indexOf(fruit);

    if (index >= 0) {
      this.fruits.splice(index, 1);
    }
  }

  getValue(fruit : Fruit): void{
    
    const val = this.fruits.length;
    const varr = [];
    for(var i=0; i < val; i++ ){
      varr.push(this.fruits[i].name)
    }
    console.log(varr);
  }

}
