const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken')

var hospitalSchema = new mongoose.Schema({
    name: {
        type: String,
        required: 'name can\'t be empty',

    },
    location: {
        type: String
    },
    branch: {
        type : Array,
        required: 'branch is required',
    },
    address: {
        type: String,
        required: 'name can\'t be empty',
    }
});


// userSchema.methods.generateJwt = function () {
//     return jwt.sign({ _id: this._id},
//         process.env.JWT_SECRET,
//     {
//         expiresIn: process.env.JWT_EXP
//     });
// }

mongoose.model('Hospital', hospitalSchema);