const mongoose = require('mongoose');

mongoose.connect("mongodb://localhost:27017/healthAppDB",{useNewUrlParser: true}, (err)=>{
    if(!err){ 
        console.log("connection to mongo database successful")
    }
    else{   
        console.log("We cannot connect to mongo databse :" + JSON.stringify(err, undefined, 2))
    };   
}); 

require('./user.model'),
require('./admin.model'),
require('./hospital.model'),
require('./patient.model')
    // mongoose.connect(process.env.MONGODB_URI, (err)=>{