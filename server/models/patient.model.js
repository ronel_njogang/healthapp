const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken')

var patientSchema = new mongoose.Schema({
    fullName: {
        type: String,
        required: 'Full name can\'t be empty',
        unique : true
    },
    
    address: {
        type: String,
        required: 'Your address is required ',
    },
    
    phoneNumber: {
        type : String,
        required: 'Phone number is required',
        unique: true
    },
    dateOfBirth: {
        type: String,
        required: 'please enter your date of birth',
    },
    email: {
        type: String,
        required: 'Email can\'t be empty',
        unique: true
    },


});

// Custom validation for email
patientSchema.path('email').validate((val) => {
    emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return emailRegex.test(val);
}, 'Invalid e-mail.');

mongoose.model('Patient', patientSchema);