const passport = require('passport');
const localStrategy = require('passport-local').Strategy;
const mongoose = require('mongoose');


var Doctor = mongoose.model('User');
var Admin = mongoose.model('admin')
passport.use('admin-login-local',

    //authentification function. Check if the given credentials are in mongoDB
    new localStrategy({ usernameField: 'email', passwordField: 'password' },
        (username, password, done) => {
            Admin.findOne({ email: username },
                (err, admin) => {
                    //If we get an error, we return it
                    if (err)
                        return done(err);

                    // In case of unknown email, return a message
                    else if (!admin)
                        return done(null, false, { message: 'email is not registered' });

                    // If the entered password is wrong, return a message
                    else if (!admin.verifyPassword(password))
                        return done(null, false, { message: 'Wrong password.' });
                        
                    // authentication succeeded
                    else
                        return done(null, admin);
                });
        })

)
passport.use('user-login-local',

    //authentification function. Check if the given credentials are in mongoDB
    new localStrategy({ usernameField: 'email', passwordField: 'password' },
        (username, password, done) => {
            Doctor.findOne({ email: username },
                (err, doctor) => {
                    //If we get an error, we return it
                    if (err)
                        return done(err);

                    // In case of unknown email, return a message
                    else if (!doctor)
                        return done(null, false, { message: 'email is not registered' });

                    // If the entered password is wrong, return a message
                    else if (!doctor.verifyPassword(password))
                        return done(null, false, { message: 'Wrong password.' });
                        
                    // authentication succeeded
                    else
                        return done(null, doctor);
                });
        })

)