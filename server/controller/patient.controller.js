const mongoose = require('mongoose');
const Patient = mongoose.model('Patient');
const passport = require('passport')

module.exports.savePatient = (req, res, next) =>{
    console.log('inside add patient function');
    var patient = new Patient();
    patient.fullName = req.body.fullName;
    patient.address = req.body.address;
    patient.phoneNumber = req.body.phoneNumber;
    patient.dateOfBirth = req.body.dateOfBirth;
    patient.email = req.body.email;
    
    patient.save((err, doc) => {
        if (!err)
            res.send(doc); 
        else
        if (err.code == 11000)
            res.status(422).send(['Duplicate email adrress found.'+ err.code]);
        else
            return next(err);
    })
}
