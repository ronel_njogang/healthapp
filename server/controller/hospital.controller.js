const mongoose = require('mongoose');
const Hospital = mongoose.model('Hospital');
const passport = require('passport')

module.exports.saveHospital = (req, res, next) =>{
    console.log('inside save hospital function');
    var hospital = new Hospital();
    hospital.name = req.body.name;
    hospital.location = req.body.location;
    hospital.branch = req.body.branch;
    hospital.address = req.body.address;
    
    hospital.save((err, doc) => {
        if (!err)
            res.send(doc)
        else
        if (err.code == 11000)
            res.status(422).send(['Duplicate values found.'+ err.code]);
        else
            return next(err);
    })
}


module.exports.getHospitals = (req, res, next)=>{
    console.log('you want to get all hospital ?');
    
    hospital.find((error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
    })
}