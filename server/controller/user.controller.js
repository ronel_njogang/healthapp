const mongoose = require('mongoose');
const User = mongoose.model('User');
const passport = require('passport')

module.exports.register = (req, res, next) =>{
    console.log('inside register function');
    var user = new User();
    user.fullName = req.body.fullName;
    user.branch = req.body.branch;
    user.statut = req.body.statut
    user.phoneNumber = req.body.phoneNumber;
    user.email = req.body.email;
    user.password = req.body.password;
    
    user.save((err, doc) => {
        if (!err)
            res.send(doc); 
        else
        if (err.code == 11000)
            res.status(422).send(['Duplicate email adrress found.'+ err.code]);
        else
            return next(err);
    })
}

module.exports.authenticate = (req, res, next) => {
    // call for passport authentication

    passport.authenticate('user-login-local', (err, doctor, info) => {       
        // error from passport middleware
        if (err) return res.status(400).json(err);
        // registered user
        else if (doctor) return res.status(200).json({ "token": doctor.generateJwt() });
        // unknown user or wrong password
        else return res.status(404).json(info);
    })(req, res);
}