const mongoose = require('mongoose');
const Admin = mongoose.model('admin');
const passport = require('passport') 

module.exports.adminRegister = (req, res, next) =>{
    console.log('inside adminRegister function');
    var admin = new Admin();
    admin.fullName = req.body.fullName;
    admin.email = req.body.email;
    admin.password = req.body.password;
    
    // saving datas taken from the view
    admin.save((err, doc) => {
        if (!err)
            res.send(doc);
        else
        if (err.code == 11000)
            res.status(422).send(['Duplicate email adrress found.'+ err.code]);
        else
            return next(err);
    })
}

module.exports.adminAuthenticate = (req, res, next) => {
    // call for passport authentication

    passport.authenticate('admin-login-local', (err, admin, info) => {       
        // error from passport middleware
        if (err) return res.status(400).json(err);
        // registered admin
        else if (admin) return res.status(200).json({ "token": admin.generateJwt() });
        // unknown admin or wrong password
        else return res.status(404).json(info);
    })(req, res);
}