const express = require('express'); 
const router = express.Router();
const ctrlUser = require('../controller/user.controller');
const ctrlAdmin = require('../controller/admin.controller');
const ctrlPatient = require('../controller/patient.controller');
const ctrlHospital = require('../controller/hospital.controller')

//register an user
router.post('/register', ctrlUser.register);
router.post('/adminRegister', ctrlAdmin.adminRegister);

//authenticate an user
router.post('/authenticate', ctrlUser.authenticate);
router.post('/adminAuthenticate', ctrlAdmin.adminAuthenticate);

//save  datas or entries
router.post('/addHospital' , ctrlHospital.saveHospital);
router.post('/addPatient' , ctrlPatient.savePatient);

//get data saved is database
router.get('/getHospitals', ctrlHospital.getHospitals)

module.exports = router; 